import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    SidebarModule,
    ButtonModule,
    TooltipModule
  ],
  exports: [
  	CartComponent
  ]
})
export class CartModule { }
