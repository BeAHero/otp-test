import { Component, OnInit } from '@angular/core';
import { WhatTheCartContainsService } from '../services/what-the-cart-contains.service';
import { BookDetailsService } from '../services/book-details.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	numberOfItems: number = 0;
	theContentOfCart;

  constructor( private whatTheCartContainsService: WhatTheCartContainsService, private bookDetailsService: BookDetailsService ) { }

  ngOnInit() {
  	this.whatTheCartContainsService.castLenght.subscribe(
  		lenghtOfCartItems => this.numberOfItems = lenghtOfCartItems
  	);
  	
  	this.whatTheCartContainsService.castItemPutToCart.subscribe(
  		linksInsideTheCart => this.theContentOfCart = linksInsideTheCart
  	);
  }

  deleteIthem(item) {
  	this.whatTheCartContainsService.deleteCartItem(item);
  }

  openDetaisl(theBookUrl) {
  	this.bookDetailsService.displayModal = true;
    this.bookDetailsService.calledUrl = theBookUrl;
  }

}
