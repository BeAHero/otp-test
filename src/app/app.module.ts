import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { SimpleSearchModule } from './simple-search/simple-search.module';
import { ItemListModule } from './item-list/item-list.module';
import { CartModule } from './cart/cart.module';
import { WhatTheCartContainsService } from './services/what-the-cart-contains.service';

import { ProductDetailModule } from './product-detail/product-detail.module';
import { BookDetailsService } from './services/book-details.service';
import { TabViewModule } from 'primeng/tabview';
import { SimultaneouslyModule } from './simultaneously/simultaneously.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SimpleSearchModule,
    ItemListModule,
    CartModule,
    ProductDetailModule,
    TabViewModule,
    SimultaneouslyModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
