import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimultaneouslySearchService {

  constructor(private http: HttpClient) { }

   public requestDataFromMultipleSources(inputs): Observable<any[]> {

   let responses = []

    let responseListLength = inputs.length;

	for (let i = 0; i < responseListLength; i++) {
		responses[i] = this.http.get("https://www.googleapis.com/books/v1/volumes?q="+inputs[i]);
	}
  
    return forkJoin(responses);
  }
}
