import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SimpleSearchItem } from '../interfacer/simple-search-item';

@Injectable({
  providedIn: 'root'
})
export class BookDetailsService {
	displayModal: boolean = false;
	calledUrl;

  constructor(private http: HttpClient) {

  }

    getBookDetails(): Observable<SimpleSearchItem[]> {
    // this.displayModal = true;
    return this.http.get<SimpleSearchItem[]>(this.calledUrl);
    }
}
