import { TestBed } from '@angular/core/testing';
import { SimpleSearchService } from './simple-search.service';

describe('SimpleSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimpleSearchService = TestBed.get(SimpleSearchService);
    expect(service).toBeTruthy();
  });
});
