import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SimpleSearchItem } from '../interfacer/simple-search-item';

@Injectable({
  providedIn: 'root'
})
export class SimpleSearchService {

  constructor(private http: HttpClient) { }

  getSimpleSearch(whatWesearch): Observable<SimpleSearchItem[]> {
    return this.http.get<SimpleSearchItem[]>('https://www.googleapis.com/books/v1/volumes?q='+whatWesearch);
    }
}
