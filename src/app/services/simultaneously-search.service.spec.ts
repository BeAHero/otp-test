import { TestBed } from '@angular/core/testing';

import { SimultaneouslySearchService } from './simultaneously-search.service';

describe('SimultaneouslySearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimultaneouslySearchService = TestBed.get(SimultaneouslySearchService);
    expect(service).toBeTruthy();
  });
});
