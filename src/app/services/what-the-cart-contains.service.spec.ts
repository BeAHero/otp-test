import { TestBed } from '@angular/core/testing';

import { WhatTheCartContainsService } from './what-the-cart-contains.service';

describe('WhatTheCartContainsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WhatTheCartContainsService = TestBed.get(WhatTheCartContainsService);
    expect(service).toBeTruthy();
  });
});
