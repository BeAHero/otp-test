import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WhatTheCartContainsService {

	whatTheCartContains: Array<Object> = [];
	lenghtOfCartItems = new BehaviorSubject<number>(0);
	castLenght = this.lenghtOfCartItems.asObservable();

	itemPutToCart = new BehaviorSubject<any>('');
	castItemPutToCart = this.itemPutToCart.asObservable();

  constructor() {
  	if (localStorage.getItem("cartContent")) {
  		this.whatTheCartContains = this.getTheCartContetnt();
  		this.updateFunctions();
  	}
  }

  pushItemToCartObject(cartItem) {
  	this.whatTheCartContains.push(cartItem);
  	localStorage.setItem("cartContent", JSON.stringify(this.whatTheCartContains));
  	this.updateFunctions();
  }

  updateFunctions() {
  	this.lenghtOfCartItems.next(this.whatTheCartContains.length);
  	this.itemPutToCart.next(this.whatTheCartContains);
  }

  cartObjectLenght() {
  	return this.whatTheCartContains.length;
  }

  getTheCartContetnt() {
  	return JSON.parse(localStorage.getItem("cartContent"));
  }

  deleteCartItem(index){
  	this.whatTheCartContains.splice(index, 1);
  	localStorage.setItem("cartContent", JSON.stringify(this.whatTheCartContains));
  	this.updateFunctions();
  }
}
