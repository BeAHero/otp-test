import { Component } from '@angular/core';
import { SimpleSearchComponent } from './simple-search/simple-search.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'otp-test';
	SearchResults;
	resultsSimultaneouslySearch;

  handelSearchValue(event) {
  	this.resultsSimultaneouslySearch =false;
  	this.SearchResults = event;
    console.log(this.SearchResults);
  }

  simultaneouslySearchHandel(event) {
  	this.SearchResults = false;
  	this.resultsSimultaneouslySearch = event;
  }
}
