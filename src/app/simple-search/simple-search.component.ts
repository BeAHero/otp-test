import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SimpleSearchService } from '../services/simple-search.service';

@Component({
  selector: 'app-simple-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./simple-search.component.scss']
})
export class SimpleSearchComponent implements OnInit {

	whatWesearch;
	@Output() results = new EventEmitter();

	constructor(private simpleSearchService: SimpleSearchService) { }

	ngOnInit() {

	}

    getSimpleSearch() {
        this.simpleSearchService.getSimpleSearch(this.whatWesearch)
        	.subscribe(data => this.results.emit(data) );
	}

}