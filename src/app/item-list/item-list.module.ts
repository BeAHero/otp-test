import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemListComponent } from './item-list.component';
import { ButtonModule } from 'primeng/button';
import { ProductDetailModule } from '../product-detail/product-detail.module';
import { AddCartBtnModule } from '../add-cart-btn/add-cart-btn.module';

@NgModule({
  declarations: [ItemListComponent],
  imports: [
    CommonModule,
    ButtonModule,
    ProductDetailModule,
    AddCartBtnModule
  ],
  exports: [
    ItemListComponent
  ]
})
export class ItemListModule { }
