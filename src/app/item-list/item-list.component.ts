import { Component, OnInit, Input } from '@angular/core';
import { BookDetailsService } from '../services/book-details.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

	 @Input() SearchResults;
	 displayDetails: boolean;
	 detailUrl;

  constructor(private bookDetailsService: BookDetailsService) { }

  ngOnInit() {
  }

   showDetails(theBookUrl) {
    this.bookDetailsService.displayModal = true;
    this.bookDetailsService.calledUrl = theBookUrl;
  }

}
