import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SimultaneouslySearchService } from '../services/simultaneously-search.service';

@Component({
  selector: 'app-simultaneously',
  templateUrl: './simultaneously.component.html',
  styleUrls: ['./simultaneously.component.scss']
})
export class SimultaneouslyComponent implements OnInit {

    searchInputValue;
    inputs = [];
    @Output() resultsSimultaneouslySearch = new EventEmitter();

  constructor(private dataService: SimultaneouslySearchService) { }

  ngOnInit() {
  }

  searchSimultaneously() {
    
    if(this.searchInputValue.includes(",") && this.searchInputValue != "") {
       let result;
      this.searchInputValue = this.searchInputValue.split(","); 

       let arrayLength = this.searchInputValue.length;
        for (let i = 0; i < arrayLength; i++) {
          this.inputs[i] = this.searchInputValue[i];
        }

        this.dataService.requestDataFromMultipleSources(this.inputs).subscribe(responseList => {
          this.resultsSimultaneouslySearch.emit(responseList);
        });

    }

	}
}