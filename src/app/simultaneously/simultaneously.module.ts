import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimultaneouslyComponent } from './simultaneously.component';
import { ButtonModule } from 'primeng/button';
import { SimultaneouslySearchService } from '../services/simultaneously-search.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [SimultaneouslyComponent],
  imports: [
    CommonModule,
    ButtonModule,
    FormsModule
  ],
  exports: [
    SimultaneouslyComponent
  ]
})

export class SimultaneouslyModule { }
