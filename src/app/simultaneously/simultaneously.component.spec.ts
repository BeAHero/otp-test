import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimultaneouslyComponent } from './simultaneously.component';

describe('SimultaneouslyComponent', () => {
  let component: SimultaneouslyComponent;
  let fixture: ComponentFixture<SimultaneouslyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimultaneouslyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimultaneouslyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
