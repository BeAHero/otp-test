import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookDetailsService } from '../services/book-details.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  result;

  constructor(private bookDetailsService: BookDetailsService) { }

  ngOnInit() {
  }

  openDeteils() {
    this.bookDetailsService.getBookDetails()
          .subscribe(data => this.result = data );
  }


}
