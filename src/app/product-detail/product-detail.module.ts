import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductDetailComponent } from './product-detail.component';
import { DialogModule } from 'primeng/dialog';
import { AddCartBtnModule } from '../add-cart-btn/add-cart-btn.module';

@NgModule({
  declarations: [ProductDetailComponent],
  imports: [
    CommonModule,
    DialogModule,
    AddCartBtnModule
  ],
  exports: [
    ProductDetailComponent
  ]
})
export class ProductDetailModule { }
