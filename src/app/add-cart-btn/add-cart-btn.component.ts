import { Component, OnInit, Input } from '@angular/core';
import { WhatTheCartContainsService } from '../services/what-the-cart-contains.service';

@Component({
  selector: 'app-add-cart-btn',
  templateUrl: './add-cart-btn.component.html',
  styleUrls: ['./add-cart-btn.component.scss']
})
export class AddCartBtnComponent implements OnInit {
@Input() selfLink;
@Input() embeddable;

  constructor( private whatTheCartContainsService: WhatTheCartContainsService ) { }

  ngOnInit() {
  }

  putToCart(selfLink) {
  	this.whatTheCartContainsService.pushItemToCartObject(selfLink);
  }
}
