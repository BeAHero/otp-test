import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCartBtnComponent } from './add-cart-btn.component';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [AddCartBtnComponent],
  imports: [
    CommonModule,
    ButtonModule
  ],
  exports: [
  	AddCartBtnComponent
  ]
})
export class AddCartBtnModule { }
